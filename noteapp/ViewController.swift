import PDFKit
import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var paper: PDFView!

    override func viewDidLoad() {
        
        super.viewDidLoad()

        guard let path = Bundle.main.url(forResource: "pdf-file", withExtension: "pdf")
        else {
            return
        }

        paper.addGestureRecognizer(PDFDrawingGestureRecognizer(paper: paper))

        if let document = PDFDocument(url: path) {
            paper.autoScales = true
            paper.scaleFactor = 50
            paper.document = document
            
            let mockTest: PDFDrawingGestureRecognizer = PDFDrawingGestureRecognizer(paper: paper)
            mockTest.testTouchesBegan(MockTouch(first: TouchData(x: 371.5, y: 221.0)))
            mockTest.testTouchesMoved(MockTouch(first: TouchData(x: 405, y: 192.0)))
            mockTest.testTouchesEnded(MockTouch(first: TouchData(x: 437.5, y: 221.0)))
        }

    }

}

class PDFDrawingGestureRecognizer: UIGestureRecognizer {

    weak var paper: PDFView!
    private var lastPoint = CGPoint()
    private var currentAnnotation: PDFAnnotation?
    private var currentPage: PDFPage?

    init(paper: PDFView) {
        super.init(target: nil, action: nil)
        self.paper = paper
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        if let touch = touches.first {

            if touch.type != UITouch.TouchType.pencil {
                return
            }

            state = .began

            let position = touch.location(in: paper)
            guard let page = paper.page(for: position, nearest: true) else { return }
            
            currentPage = page

            let convertedPoint = paper.convert(position, to: currentPage!)
            lastPoint = convertedPoint

        } else {
            state = .failed
        }
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {

        let touch = touches.first
        
        if touch?.type != UITouch.TouchType.pencil {
            return
        }

        state = .changed

        guard let position = touches.first?.location(in: paper) else { return }
        guard let page = currentPage else { return }
        
        let convertedPoint = paper.convert(position, to: page)

        let path = UIBezierPath()
        path.move(to: lastPoint)
        path.addLine(to: convertedPoint)
        
        lastPoint = convertedPoint

        if currentAnnotation == nil {
            let border = PDFBorder()
            border.lineWidth = 3
            border.style = .solid

            currentAnnotation = PDFAnnotation(
                bounds: paper.bounds, forType: .ink,
                withProperties: [
                    PDFAnnotationKey.border: border,
                    PDFAnnotationKey.color: UIColor.black,
                    PDFAnnotationKey.interiorColor: UIColor.black,
                ]
            )
            
            let pageIndex = paper.document!.index(
                for: paper.page(for: position, nearest: true)!
            )
            
            paper.document?.page(at: pageIndex)?.addAnnotation(currentAnnotation!)
        }

        currentAnnotation!.add(path)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {

        let touch = touches.first
        
        if touch?.type != UITouch.TouchType.pencil {
            return
        }

        guard let position = touches.first?.location(in: paper) else {
            state = .ended
            return
        }
        
        guard let page = currentPage else { return }
        let convertedPoint = paper.convert(position, to: page)

        let path = UIBezierPath()
        path.move(to: lastPoint)
        path.addLine(to: convertedPoint)

        currentAnnotation?.add(path)
        currentAnnotation = nil

        state = .ended
    }
    
    func testTouchesBegan(_ touches: MockTouch) {}
    
    func testTouchesMoved(_ touches: MockTouch) {}
    
    func testTouchesEnded(_ touches: MockTouch) {}
}

// ตั้งแต่บรรทัดที่ 153 ลงมาเป็น Code ที่ใช้ทดสอบและวัดผล ไม่มีผลกับส่วนอื่น
// สามารถ Ignore Code ข้างล่างนี้ได้เลย
class MockTouch {
    var first: TouchData!

    init(first: TouchData) {
        self.first = first
    }
}

class TouchData {
    
    var x: CGFloat
    var y: CGFloat
    var type: UITouch.TouchType
    
    init(x: CGFloat, y: CGFloat) {
        self.x = x
        self.y = y
        self.type = UITouch.TouchType.pencil
    }
    
    func location(in: PDFView) -> (CGPoint) {
        return CGPoint(x: x, y: y)
    }
}
